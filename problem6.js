var resultDisplay = document.getElementById('problem6Answer');
var result = function() {
	var answer = 0;
	var limit = 100;
	var sumOfSquares = 0;
   
	for( i=1; i <= limit; i++ ) {
		var square = Math.pow(i,2);
		sumOfSquares = sumOfSquares + square;
		answer = answer + i;
	}

   
	return Math.pow(answer,2) - sumOfSquares;
};
 
resultDisplay.textContent = result();