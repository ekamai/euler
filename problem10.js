/*
:/
*/
var resultDisplay = document.getElementById('problem10Answer');
var result = function() {
	var answer = 5;
	var limit = 2000000;
	
	for( i = 5; i < limit; i++ ) {
		var isPrime = true;

		for( j = 2; j <= Math.sqrt(i) && isPrime; j++ )
		{
			if( i % j == 0 ) {
				isPrime = false;
			}
		}
		
		if( isPrime ) {
			answer = answer + i;
		}
	}
	
	return answer;
};
 
resultDisplay.textContent = result();