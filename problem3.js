var resultDisplay = document.getElementById('problem3Answer');
var result = function() {
	var maxPrimeFactor = 0;
	var number = 600851475143;
   
	for( i = Math.round(Math.sqrt(number)); maxPrimeFactor == 0 && i > 3; i-- ) {
		var potentialMaxPrimeFactor = i;

		if( number % potentialMaxPrimeFactor == 0 &&
			potentialMaxPrimeFactor % 2 != 0 &&
			potentialMaxPrimeFactor % 3 != 0 &&
			potentialMaxPrimeFactor % 5 != 0 &&
			potentialMaxPrimeFactor % 7 != 0 &&
			potentialMaxPrimeFactor % 11 != 0 ) {
				
				var isPrime = true;
			   
				for( j = Math.round(Math.sqrt(potentialMaxPrimeFactor)); j > 1 && isPrime; j-- ) {
					if( potentialMaxPrimeFactor % j == 0 ) {
						isPrime = false;
					}
				}
			   
				if( isPrime ) {
					maxPrimeFactor = potentialMaxPrimeFactor;
				}
		}
	}

	return maxPrimeFactor;
};
 
resultDisplay.textContent = result();