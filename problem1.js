var resultDisplay = document.getElementById('problem1Answer');
var result = function() {
	var multiple1 = 3;
	var multiple2 = 5;
	var limit = 1000;
	var acc = 0;
	
	for(i=1; i < limit; i++) {
		if( i % multiple1 == 0 || i % multiple2 == 0 ) {
			acc += i;
		} 
	}
	
	return acc;
};

resultDisplay.textContent = result();