var resultDisplay = document.getElementById('problem2Answer');
var result = function() {
	var multiple = 2;
	var prevTerm = 1;
	var curTerm = 1;
	var limit = 4000000;
	var acc = 0;
	
	while( curTerm < limit ) {
		if( curTerm % multiple == 0 ) {
			acc += curTerm;
		}
		var nextTerm = curTerm + prevTerm;
		prevTerm = curTerm;
		curTerm = nextTerm;
	}
	return acc;
};

resultDisplay.textContent = result();