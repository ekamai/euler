var resultDisplay = document.getElementById('problem4Answer');
var result = function() {
	var answer = 0;

	for( i = 500; i < 1000; i++ ) {
		for( j = 500; j < 1000; j++ ) {
			var result = i * j;
			var firstSequence = result.toString().substring(0,3);
			var secondSequence = result.toString().substring(3,6);
			var reversedSecondSequence = secondSequence.charAt(2) + secondSequence.charAt(1) + secondSequence.charAt(0);
			
			if(firstSequence === reversedSecondSequence && result > answer ) {
				answer = result;
			}
		}
	}
	
	return answer;
};
 
resultDisplay.textContent = result();