/*
:/
*/
var resultDisplay = document.getElementById('problem7Answer');
var result = function() {
	var answer = 0;
	var primeSequenceStop = 10001;
	var currentPrimeCounter = 2;
	
	for( i = 3; currentPrimeCounter <= primeSequenceStop; i++ ) {
		var isPrime = true;
		
		for( j = 2; j < i && isPrime; j++ ) {
			if( i % j == 0 ) {
				isPrime = false;
			}
		}
		
		if( isPrime === true ) {
			currentPrimeCounter = currentPrimeCounter + 1;
			answer = i;
		}
	}
	
	return answer;
};
 
resultDisplay.textContent = result();