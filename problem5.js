/*
:/
*/
var resultDisplay = document.getElementById('problem5Answer');
var result = function() {
	var limit = 20;
	var answer = limit;
   
	for( i=limit; i > 1; i-- ) {
		var prevAnswer = answer;
		while(answer % i != 0) {
			answer = answer + 1;
		}
	   
		if( prevAnswer != answer ) {
			i = limit;
		}
	}

	return answer;
};
 
resultDisplay.textContent = result();