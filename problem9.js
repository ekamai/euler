var resultDisplay = document.getElementById('problem9Answer');
var result = function() {
	var answer = 0;
	var tripletSum = 1000;
	var a,b,c;
	
	for( i=2; i < tripletSum; i++ ) {
		for( j=1; j < i; j++ ) {
			var curSum = j + i + Math.sqrt(Math.pow(i,2)+Math.pow(j,2));
			if( curSum === tripletSum ) {
				a = j;
				b = i;
			}
		}
	}
	
	c = Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
	answer = a * b * c;
	
	return answer;
};
 
resultDisplay.textContent = result();